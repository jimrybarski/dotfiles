# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

if [ $(which redshift) ]; then
    redshift -o -P -O 5000 -b 1.0
fi

export PATH="$HOME/.local/neovim/bin:$HOME/Opfi/lib/pilercr1.06:$HOME/.cargo/bin:$PATH"

# Allow custom key combinations to type special characters
export GTK_IM_MODULE="gtk-im-context-simple"

if [ -e "$HOME/.customprofile" ]; then
    source "$HOME/.customprofile" 
fi

# Clear the downloads directory at boot
# If you realize you have something that can't be replaced, log in as root in recovery mode and move the file to another directory
if [ -e "$HOME/downloads" ]; then
    rm -rf $HOME/downloads/*
fi
