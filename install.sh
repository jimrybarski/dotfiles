#!/bin/bash
set -euo pipefail

for filename in `find . -type f | cut -c 3- | grep -v '\.git/' | grep -v "install.sh"`; do
    echo "$filename"
    if [ -z $filename ]; then
	    echo "Error! Empty filename. Something is very wrong with this script."
        exit 1
    fi
    rm -f "$HOME/$filename"
    mkdir -p $(dirname "$HOME/$filename")
	ln -s "$PWD/$filename" ~/$filename
    #echo "linked $filename"
done

# Install any new neovim plugins
$(which nvim) --headless +PlugInstall +qa
