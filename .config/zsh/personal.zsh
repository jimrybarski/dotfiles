# configs that only apply to personal desktop and laptop computers
OUT="/stor/home/rybarskj/Metagenomics_CRISPR_transposons/output/nontn7"

# servers
alias sf="ssh freeboxroot"
alias sm="ssh marble"
alias ss="ssh stampede"
alias sw="ssh wilke"
alias sw2="ssh wilke2"

function snapgene() {
    bash $HOME/scripts/snapgene.sh $1
}

alias sg=snapgene

function backup() {
    bash $HOME/scripts/add-to-backup-list.sh $1    
}

function wallpaper() {
  directory=$(pwd -P)
  current_symlink=$HOME/pictures/wallpaper-images/current-image
  rm -f $current_symlink
  ln -s $directory/$1 $current_symlink
}

function mp3() {
    $HOME/.local/bin/youtubedl-env/bin/youtube-dl --extract-audio --audio-format mp3 "$1" -o "~/music/%(title)s.%(ext)s"
}

function yv() {
    $HOME/.local/bin/youtubedl-env/bin/youtube-dl "$1" -o "$HOME/pvid/%(title)s.%(ext)s"
}

function o() {
    if [ $# -eq 0 ]; then
        xdg-open .  > /dev/null 2>&1
    else
        xdg-open "$@" > /dev/null 2>&1
    fi
}

function sciproxy() {
  ssh-add ~/.ssh/id_rsa
  google-chrome-stable --proxy-server="socks5://127.0.0.1:8080" &
  ssh -CTND 127.0.0.1:8080 wilke
}

function otp() {
    code=$(pass otp/$1)
    while true; do
        sleep 1;
        oathtool --totp -b $code | xclip -i -selection clipboard
    done
}

# screen brightness
alias bright='redshift -o -P -O 5000 -b 1.0'
alias day='redshift -o -P -O 3800 -b 0.75'
alias night='redshift -o -P -O 2500 -b 0.60'
