# Set up the prompt
PROMPT="%B%F{green}%n@%m%f%b [%*] [%(?.%F{green}√%f.%F{red}%?%f)] %d %F{blue}$%f "

setopt histignorealldups sharehistory auto_cd extended_history append_history #correct correct_all 

# vi mode
bindkey -v 
# only wait 0.5 sec before entering normal mode
export KEYTIMEOUT=0.5

EDITOR=$(which vim)
export MANPAGER="sh -c 'col -bx | bat -l man -p'"

# history
HISTSIZE=1000000
SAVEHIST=1000000
HISTFILE=$HOME/.zsh_history
bindkey '^R' history-incremental-search-backward

# Use modern completion system
autoload -Uz compinit
compinit

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2

# Linux has dircolors, FreeBSD has gdircolors
dircolors_exec=$(which dircolors || which gdircolors)
eval "$($dircolors_exec -b)"

zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

export LC_ALL="en_US.UTF-8"
export LANG="en_US"
umask 027

alias n='ssh notes'

# Edit markdown notes at remote server with zsh/neovim
function edit_note () {
    filename=$1
    suffix=$2

    if [[ -z "$suffix" ]]; then
	suffix="md"
    fi
    n -t "/usr/local/bin/zsh -ic 'v $filename.$suffix'"
}
alias en=edit_note

# CLI configuration
alias ll="exa -al --header --time-style long-iso"
alias ls="exa"
alias mv="mv -i"  # always prompt if overwriting a file
alias v="vim"
alias b="bat"

#programming
alias eba=". env/bin/activate"
alias randpass="openssl rand -base64 32"

# personal
alias blog="cd $HOME/blog && zola serve"
alias blogsync="pushd $HOME/blog && zola build && bash $HOME/scripts/minify.sh $HOME/blog/public && rsync --verbose --checksum --delete --progress --recursive $HOME/blog/public/ freeboxroot:/usr/jails/rybarski.com/var/www/html/rybarski.com && ssh -p 29 freeboxroot 'chown -R www:www /usr/jails/rybarski.com/var/www/html/rybarski.com' && popd"
alias e="ssh notes 'cat /root/notes/exercise.csv' | rg -i $(date +%a) | cut -d',' -f1,3 | rg '(.*?),(\d+)' -r '\$2 \$1'"
alias cfe="edit_note exercise csv"

# notes
alias lg="edit_note log"
alias todo="edit_note todo"
alias tv="edit_note tv"
alias comedians="edit_note comedians"
alias gripes="edit_note unix_gripes"
alias cast="edit_note cast"
alias ol="edit_note oligos csv"
alias sch="edit_note sch"
alias travel="edit_note checklist-travel"

# date calculators
alias ddiff="dateutils.ddiff -f '%Y years %d days'"
alias daysuntil="dateutils.ddiff -f '%Y years %d days' today"

# shortcuts to edit configuration files
alias cf="rg 'alias cf' $HOME/.zshrc $HOME/.config/zsh/*"
alias cfz="v $HOME/.zshrc"
alias cfzc="v $HOME/.config/zsh/$HOST.zsh"
alias cfzp="v $HOME/.config/zsh/personal.zsh"
alias cfi3="v $HOME/.config/i3/config"
alias cfib="v $HOME/.config/i3blocks/config"
alias cfa="v $HOME/.config/alacritty.yml"
# alias cfv="v $HOME/.config/nvim/init.vim"
alias cfv="v $HOME/.vimrc"
alias cfs="v $HOME/.ssh/config"

# system management
alias reload="source $HOME/.zshrc"
alias plug='vim +silent +"PlugInstall --sync" +qa && nvim +silent +"PlugClean" +qa'
alias upgrade='sudo apt update && sudo apt upgrade -y && sudo apt-get autoremove'

# git 
alias pull='git pull origin main'
alias pullm='git pull origin master'
alias push='git push origin main'
alias pushm='git push origin master'
alias gits='git status'
alias gitc="GIT_EDITOR='vim +startinsert' git commit"
alias gitcs="GIT_EDITOR='vim +startinsert' git commit -S"

# science
alias gel="java -jar $HOME/bin/GelAnalyzer2010a/GelAnalyzer.jar"
alias imagej='/opt/Fiji.app/ImageJ-linux64'
alias jek='bundle exec jekyll serve'
alias len="$HOME/code/seqtools-cli/target/release/seqtools-cli len"
alias r="$HOME/code/seqtools-cli/target/release/seqtools-cli r"
alias rc="$HOME/code/seqtools-cli/target/release/seqtools-cli rc"
alias gc="$HOME/code/seqtools-cli/target/release/seqtools-cli gc"

function fixspaces() {
    # replace spaces in filenames with underscores
    for file in *' '*; 
        do mv -- "$file" "${file// /_}"; 
    done
}

function mvf () {
    directory=${@: -1}
    mv $@
    pushd $directory
}

# ============================================
#       KEEP THIS AT THE VERY END
# ============================================
# Load machine-specific config files
custom_config="$HOME/.config/zsh/$HOST.zsh"
if [[ -e $custom_config ]]; then
    source $custom_config
else
    echo "No custom config"
fi

# Make terminal compatible with FreeBSD and Linux servers
export TERM=xterm-256color zsh
export GPG_TTY=$(tty)
export REVIEW_BASE="master"
